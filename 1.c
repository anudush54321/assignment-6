#include <stdio.h>

int num(int n){
    if (n==0){
        return 0;
    }
    else{
            printf("%d",n);
            num(n-1);
    }
}

int pattern(int n){
    if (n==0){
        return 0;
    }
    else{
        pattern(n-1);
        num(n);
        printf("\n");
            }
}

int main(){
    int n;
    printf("Enter a number:");
    scanf("%d",&n);
    pattern(n);
    return 0;
}
